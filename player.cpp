#include "player.h"
#include <limits>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    this->side = side;
    board = Board();
    
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/** 
 * sets the player's board
 */
void Player::setBoard(Board board)
{
    this->board = board;
}

/**
 * minimax helper function
 */
int Player::helper(Board *board, int depth) {
	Side tempside;
	if(depth % 2 == 0)
		tempside = side;
	else
		tempside = (Side) !side;
	
	//base case
	if(testingMinimax && depth == 2)
	{
	    if(side == WHITE)
	        return board->countWhite() - board->countBlack();
	    else
	        return board->countBlack() - board->countWhite();
	}
	else if(depth == 4)
	{        
	    if(board->countWhite() + board->countBlack() >= 40)
	    {
	        if(side == WHITE)
	            return board->countWhite() - board->countBlack();
	        else
	            return board->countBlack() - board->countWhite();
	    }    
	    return board->getSideScore(side) - board->getSideScore((Side) !side);
	}
	
	vector<Move> moves = board->getMoves(tempside);
	
	int minScore = std::numeric_limits<int>::max();
	
	for(unsigned i = 0; i < moves.size(); i++)
	{
		Board *copy = board->copy();
		Move *move = new Move(moves[i]);
		copy->doMove(move, tempside);
		delete move;
		int score = helper(copy, depth + 1);
		if(score < minScore)
			minScore = score;
		delete copy;
	}
	
	return minScore;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    board.doMove(opponentsMove, (Side) !side);
    
    vector<Move> moves = board.getMoves(side);
    
    if(moves.size() == 0)
        return NULL;
        
    Move best(0,0);
	int maxScore = std::numeric_limits<int>::min();
	
    for(unsigned i = 0; i < moves.size(); i++)
	{
		Board *copy = board.copy();
		Move *move = new Move(moves[i]);
		copy->doMove(move, side);
		delete move;
		int score = helper(copy, 1);
		if(score > maxScore)
		{
			maxScore = score;
			best = moves[i];
		}
		delete copy;
	}
    
    Move *move = new Move(best);
    board.doMove(move, side);
    return move;
}
