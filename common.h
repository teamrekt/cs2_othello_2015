#ifndef __COMMON_H__
#define __COMMON_H__

enum Side { 
    WHITE = 0, BLACK = 1
};

class Move {
   
public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    Move(const Move& other) {
        this->x = other.x;
        this->y = other.y;
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
};

#endif
