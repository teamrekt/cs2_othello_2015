#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    void setBoard(Board board);
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int helper(Board *board, int depth);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

private:
    Side side;
    Board board;    
};

#endif
